# templates websites bootstrap

Collection of FREE templates for build websites.  
Most are made with Bootstrap Twitter.

Gitlab Pages : 

> https://v20100t.gitlab.io/templates-websites-bootstrap/

Templates :


OK : 

* https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/pizza-gh-pages/
* https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/food-funday-master/

_A_tester : 

## Restaurants

1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/Mamma-s-Kitchen-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/MeatKing-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/Osteriax-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/Restaurant-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/awesplash-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/bistro.1.0/bistro 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/classimax-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/eatwell-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/flatter-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/food-funday-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/foodfun 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/luigis 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/luto 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/marco 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/pato-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/pizza 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/pizza-gh-pages 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/pulse 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/restaurant/restaurant
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/restaurant-html-template-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/risotto-colorlib/risotto 
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/taste-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/thegrill-master 	
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/yummy-master 
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/_A_tester/china-bistro-free-website-template/china-bistro-free-website-template
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Restaurant/eat-restaurant-free-web-template


## Immobilier 

> Location, achat, vacances ...


1. https://v20100t.gitlab.io/templates-websites-bootstrap/Immobilier/edge-interior-free-web-template/edge-interior-free-web-template
1. https://v20100t.gitlab.io/templates-websites-bootstrap/Immobilier/golden-hotel-free-web-template
2. https://v20100t.gitlab.io/templates-websites-bootstrap/Immobilier/hillside-real-estate-free-web-template


## Admin - Dashboard


1. https://v20100t.gitlab.io/templates-websites-bootstrap/Admin/hybrid-admin-free-web-template


## Magasins - divers

1. https://v20100t.gitlab.io/templates-websites-bootstrap/Magazins/blush-beauty-salon-free-website


## Zen Yoga - Bien être

1. https://v20100t.gitlab.io/templates-websites-bootstrap/Zen-yoga/meditative/
2. https://v20100t.gitlab.io/templates-websites-bootstrap/Zen-yoga/yogalax/yogalax/

 
